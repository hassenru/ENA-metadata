tmp_date <- scan(what = "character", file="ENA_search_xml/tmp_date", sep = "\n", comment.char="")
df_xml_date <- future_map_dfr(xml_df, function(x) x[, colnames(x) %in% c("PRIMARY ID", tmp_date), drop = F])
df_xml_date <- df_xml_date[df_xml_date$'PRIMARY ID' %in% ena.out$sample_accession[(is.na(ena.out$collection_date) | ena.out$collection_date == "") & ena.out$collection_date_start == "" & ena.out$collection_date_end == ""], ]
df_xml_date <- df_xml_date[!apply(df_xml_date[, -1], 1, function(x) sum(is.na(x)) == length(x)), ]
df_xml_date <- df_xml_date[, !apply(df_xml_date, 2, function(x) sum(is.na(x)) == length(x))]
dim(df_xml_date)
colnames(df_xml_date)
### Keep:
# month_day_year
# date sampled
# sampling event, start, date/time
# year collected
# Date_collected
# Date.2
# Date.1
# DateCollectedWater
sample(unique(df_xml_date[, "collectionday"]), 10, replace = T)

tmp_primer <- scan(what = "character", file="ENA_search_xml/tmp_primer", sep = "\n", comment.char="")
df_xml_primers <- future_map_dfr(xml_df, function(x) x[, colnames(x) %in% c("PRIMARY ID", tmp_primer), drop = F])
df_xml_primers <- df_xml_primers[df_xml_primers$'PRIMARY ID' %in% ena.out$sample_accession[ena.out$target_gene == ""], ]
df_xml_primers <- df_xml_primers[!apply(df_xml_primers[, -1], 1, function(x) sum(is.na(x)) == length(x)), ]
df_xml_primers <- df_xml_primers[, !apply(df_xml_primers, 2, function(x) sum(is.na(x)) == length(x))]
dim(df_xml_primers)
colnames(df_xml_primers) # 2:152
### Keep:
keep_primer <- c(
  2, 5:11, 13:14, 31:33, 38, 
  40:44, 48:49, 51:53, 56:58, 
  61, 63:64, 68:77, 79:80, 84, 
  89, 91, 94, 96, 99, 102:105,
  108, 112:113, 115, 118, 122:123,
  126:127, 130, 134, 136:140, 145:151
)
colnames(df_xml_primers)[keep_primer]
# linkerprimersequence
# target_fragment
# target.subfragment
# target.gene
# primer (f, r)
# forward_primer_515f
# reverse_primer_806r
# region_amplified
# target region
# library_contruction_protocol
# target_sequencing_region
# forward
# reverse
# amplicon target
# type_amplicon
# 16s
# seq_region
# primer_code
# marker_gene_sequence
# 16S_or_ITS
# ampliconID
# 16s_or_18s
# forwand_primer
# amplicon_region
# targeted_loci
# forward_primer_sequence
# 16S_rRNA_gene_target_region
# ReverseName
# library_consrturction
# RevPrimer
# PrimerName
# rev_primer_seq
# fwd_primer_seq
# molecular_marker
# LinkerPrimerSequenceForward
# amplicon_target
# list_of_reverse_pcr_primers
# list_of_forward_pcr_primers
# targeted_gene_or_locus_name_for_marker_gene_studies
# Loci
# 16S_variable_region
# amplicon sequencing
# forward_barcode
# gene_target
# fungal_amplicon
# marker_amplified
# PCR Primer
# reverselinker
# reverse_primer_co1
# forward_primer_trnl
# reverse_primer_trnl
# forward_primer_co1
# RCLinkerPrimerSequence
# Amplicon_target_region
# amplicons
# ForwardPrimer
# marker_gene
# ReversePrimerSequence
# Amplicon Type
# PCR_primer_rev
# PCR_primer_fwd
# Amplicon region targeted
# amplification_region
# Moleccular Marker
# Amplicon_target
# Amplicon (gene)
# forwardbarcode
# reversebarcode
# amplified gene
# Forward Primer (515F-Y)
# Forward Primer (V4F)
# Reverse Primer (V4RB)
# Reverse Primer (28SR)
# Forward Primer (5.8SF)
# target_sequence
sample(size = 10, replace = T, x = unique(df_xml_primers[, 2]))

tmp_xy <- scan(what = "character", file="ENA_search_xml/tmp_xy", sep = "\n", comment.char="")
df_xml_xy <- future_map_dfr(xml_df, function(x) x[, colnames(x) %in% c("PRIMARY ID", tmp_xy), drop = F])
df_xml_xy <- df_xml_xy[df_xml_xy$'PRIMARY ID' %in% ena.out$sample_accession[is.na(ena.out$lat) | is.na(ena.out$lon)], ]
df_xml_xy <- df_xml_xy[!apply(df_xml_xy[, -1], 1, function(x) sum(is.na(x)) == length(x)), ]
df_xml_xy <- df_xml_xy[, !apply(df_xml_xy, 2, function(x) sum(is.na(x)) == length(x))]
dim(df_xml_xy)
colnames(df_xml_xy)
### Keep:
# geographic location (lattitude)
# geographic.location.longitude
# geographic.location.latitude
# latitude [n]
# longitude [e]
# sampling event, start, latitude
# sampling event, start, longitude
# lat_lo_n
# TransectEndDecimalLatitude
# TransectBeginDecimalLatitude
# TransectBeginDecimalLongitude
# TransectEndDecimalLongitude
# SiteLatitude
# SiteLongitude
# latlon
# site_lat
# site_long
# lat_long_start
# lat_long_end
sample(size = 10, replace = T, x = unique(df_xml_xy[, 2]))

tmp_region <- scan(what = "character", file="ENA_search_xml/tmp_region", sep = "\n", comment.char="")
df_xml_region <- future_map_dfr(xml_df, function(x) x[, colnames(x) %in% c("PRIMARY ID", tmp_region), drop = F])
df_xml_region <- df_xml_region[df_xml_region$'PRIMARY ID' %in% ena.out$sample_accession[ena.out$country == ""], ]
df_xml_region <- df_xml_region[!apply(df_xml_region[, -1], 1, function(x) sum(is.na(x)) == length(x)), ]
df_xml_region <- df_xml_region[, !apply(df_xml_region, 2, function(x) sum(is.na(x)) == length(x))]
dim(df_xml_region)
colnames(df_xml_region)
### Keep:
# geographic.location
# county
# collecting_location
sample(size = 10, replace = T, x = unique(df_xml_region[, 2]))

tmp_z_2023 <- scan(what = "character", file="Update_2023/ENA_search_xml/sample_xml_parsed/sample_attributes_location_z.txt", sep = "\n", comment.char="")
tmp_z <- scan(what = "character", file="ENA_search_xml/tmp_region", sep = "\n", comment.char="")
df_xml_z <- future_map_dfr(xml_df, function(x) x[, colnames(x) %in% c("PRIMARY ID", tmp_z_2023, tmp_z), drop = F])
df_xml_z <- df_xml_z[df_xml_z$'PRIMARY ID' %in% ena.out$sample_accession[is.na(ena.out$altitude) & is.na(ena.out$depth) & is.na(ena.out$elevation)], ]
df_xml_z <- df_xml_z[!apply(df_xml_z[, -1], 1, function(x) sum(is.na(x)) == length(x)), ]
df_xml_z <- df_xml_z[, !apply(df_xml_z, 2, function(x) sum(is.na(x)) == length(x))]
unique(df_xml_z[, 2])
# not worth it to investigate further, stick to indexed parameters